﻿using System;
using System.Net;
using Newtonsoft.Json;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using RestSharp;
using System.Threading.Tasks;

namespace BakeryAwesome;

public class Tests
{

    private string getUrl = "https://61ea9982c9d96b0017700c47.mockapi.io/bakery-awesome/1";
    private string getUrl2 = "https://61ea9982c9d96b0017700c47.mockapi.io/bakery-awesome";
    HttpClient httpClient = new HttpClient();

    [Test]
    public void TestHttpResponseCodeForUrl1()
    {
        //creating http client
        HttpClient httpClient = new HttpClient();
        Uri getUri = new Uri(getUrl);
        Task<HttpResponseMessage> httpResponse = httpClient.GetAsync(getUri);
        HttpResponseMessage httpResponseMessage = httpResponse.Result;

        HttpStatusCode statusCode = httpResponseMessage.StatusCode;
        //Http status code
        Console.WriteLine("------------");
        Console.WriteLine("Http response status is:" + statusCode);
        Console.WriteLine("Http response code is:" + (int)statusCode);

        //Close the connection
        httpClient.Dispose();
    }

    [Test]
    public void TestHttpResponseCodeForUrl2()
    {
        //creating http client
        HttpClient httpClient = new HttpClient();
        Uri getUri = new Uri(getUrl2);
        Task<HttpResponseMessage> httpResponse = httpClient.GetAsync(getUri);
        HttpResponseMessage httpResponseMessage = httpResponse.Result;

        HttpStatusCode statusCode = httpResponseMessage.StatusCode;
        //Http status code
        Console.WriteLine("------------");
        Console.WriteLine("Http response status is:" + statusCode);
        Console.WriteLine("Http response code is:" + (int)statusCode);

        //Close the connection
        httpClient.Dispose();
    }

    [Test]
    public void TestPayloadElementData()
    {

        //creating http client
        HttpClient httpClient = new HttpClient();
        Uri getUri = new Uri(getUrl2);
        Task<HttpResponseMessage> httpResponse = httpClient.GetAsync(getUri);
        HttpResponseMessage httpResponseMessage = httpResponse.Result;

        Console.WriteLine(httpResponseMessage.ToString());


        //Response Data
        HttpContent responseContent = httpResponseMessage.Content;
        Task<string> responseData = responseContent.ReadAsStringAsync();
        string data = responseData.Result;
        Console.WriteLine(data);
        //Close the connection
        httpClient.Dispose();
    }
    
}

